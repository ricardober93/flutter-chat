import 'package:flutter/material.dart';

class Labels extends StatelessWidget {
  final String route;
  final String title;
  final String subtitle;
  const Labels(
      {super.key,
      required this.route,
      required this.title,
      required this.subtitle});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          width: double.infinity,
          alignment: Alignment.center,
          child: Column(
            children: [
              Text(subtitle,
                  style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                      fontSize: 15)),
              const SizedBox(
                height: 10,
              ),
              GestureDetector(
                  child: Text(
                    title,
                    style: const TextStyle(
                        color: Colors.blue, fontWeight: FontWeight.bold),
                  ),
                  onTap: () => {
                        Navigator.pushReplacementNamed(
                          context,
                          route,
                        )
                      }),
            ],
          )),
    );
  }
}
