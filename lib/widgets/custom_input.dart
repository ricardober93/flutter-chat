import 'package:flutter/material.dart';

class CustomInput extends StatelessWidget {
  final IconData icon;
  final String placeholder;
  final TextInputType keyboardType;
  final TextEditingController textController;
  final bool? isPassword; // optional

  const CustomInput(
      {required Key key,
      required this.icon,
      required this.placeholder,
      required this.keyboardType,
      required this.textController,
      this.isPassword})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 1, bottom: 1, right: 20, left: 20),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(24),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.05),
                offset: const Offset(0, 5),
                blurRadius: 5)
          ]),
      child: TextField(
          controller: textController,
          autocorrect: false,
          obscureText: isPassword ?? false,
          keyboardType: TextInputType.emailAddress,
          style: const TextStyle(color: Colors.black),
          decoration: InputDecoration(
              prefixStyle: const TextStyle(color: Colors.black),
              prefixIcon: Icon(icon, color: Colors.black),
              focusedBorder: InputBorder.none,
              border: InputBorder.none,
              hintStyle: const TextStyle(color: Colors.black),
              hintText: placeholder)),
    );
  }
}
