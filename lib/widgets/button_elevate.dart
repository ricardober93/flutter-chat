import 'package:flutter/material.dart';

class ButtonElevate extends StatelessWidget {
  final Function() onPress;
  final String? title;
  const ButtonElevate({Key? key, required this.onPress, this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: onPress,
        style: ElevatedButton.styleFrom(
            minimumSize: const Size(double.infinity, 50),
            backgroundColor: Colors.blue,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
            padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
            textStyle:
                const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        child: Text(title ?? 'Entrar'));
  }
}
