import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  final String title;
  const Logo({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          margin: const EdgeInsets.only(bottom: 20),
          width: double.infinity,
          alignment: Alignment.center,
          child: Column(
            children: [
              const Image(
                  image: AssetImage('assets/crossplane-icon.png'), width: 50),
              const SizedBox(
                height: 10,
              ),
              Text(title,
                  style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 28)),
            ],
          )),
    );
  }
}
