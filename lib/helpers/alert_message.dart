import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

alert(BuildContext context, String title, String subtitle) => {
      if (Platform.isAndroid)
        {
          showDialog(
              context: context,
              builder: (context) => AlertDialog(
                    title: Text(title),
                    content: Text(subtitle),
                    actions: [
                      MaterialButton(
                          elevation: 5,
                          textColor: Colors.blue,
                          onPressed: () => Navigator.pop(context),
                          child: const Text('Ok')),
                    ],
                  ))
        }
      else if (Platform.isIOS)
        {
          showCupertinoDialog(
              context: context,
              builder: (context) => CupertinoAlertDialog(
                    title: Text(title),
                    content: Text(subtitle),
                    actions: [
                      CupertinoDialogAction(
                          child: const Text('Ok'),
                          onPressed: () => Navigator.pop(context)),
                    ],
                  ))
        }
    };
