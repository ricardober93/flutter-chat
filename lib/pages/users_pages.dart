import 'package:chatproject/models/user.dart';
import 'package:chatproject/services/auth_services.dart';
import 'package:chatproject/services/chat_service.dart';
import 'package:chatproject/services/socket_service.dart';
import 'package:chatproject/services/user_services.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class UserPage extends StatefulWidget {
  const UserPage({super.key});

  @override
  State<UserPage> createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  final userServices = UserService();

  List<User> users = [];

  void _onRefresh() async {
    users = await userServices.getUser();
    setState(() {});
    _refreshController.refreshCompleted();
  }

  @override
  void initState() {
    _onRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context, listen: false);
    final sockerService = Provider.of<SocketServices>(context, listen: false);
    final user = authService.user!.user!;

    return Scaffold(
        appBar: AppBar(
            title: Center(
                child: Text(user.name,
                    style: const TextStyle(color: Colors.white))),
            leading: IconButton(
              icon: const Icon(Icons.logout),
              onPressed: () {
                Navigator.pushReplacementNamed(context, 'login');
                sockerService.disconnect();
                AuthService.deleteToken();
              },
            ),
            actions: [
              IconButton(
                icon: sockerService.serverStatus == ServerStatus.online
                    ? const Icon(Icons.check_circle, color: Colors.green)
                    : const Icon(Icons.check_circle, color: Colors.red),
                onPressed: () {},
              )
            ]),
        body: SmartRefresher(
          controller: _refreshController,
          onRefresh: _onRefresh,
          enablePullDown: true,
          child: _listViewUser(),
        ));
  }

  ListView _listViewUser() {
    return ListView.separated(
        itemBuilder: (context, i) => _userListItem(context, users[i]),
        separatorBuilder: (_, i) => const Divider(
              height: 1,
              color: Colors.black,
            ),
        itemCount: users.length);
  }

  ListTile _userListItem(context, User user) {
    final chatService = Provider.of<ChatService>(context, listen: false);
    
    return ListTile(
        onTap: () {
          chatService.userTo = user;
          Navigator.pushReplacementNamed(context, 'chat');
        },
        title: Text(user.name),
        subtitle: Text(user.email),
        leading: CircleAvatar(
          backgroundColor: Colors.black26,
          child: Text(user.name.substring(0, 2)),
        ),
        trailing: Container(
          width: 10,
          height: 10,
          decoration: BoxDecoration(
              color: user.online ? Colors.green : Colors.red,
              borderRadius: BorderRadius.circular(100)),
        ));
  }
}
