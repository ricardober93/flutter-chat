import 'package:chatproject/helpers/alert_message.dart';
import 'package:chatproject/services/auth_services.dart';
import 'package:chatproject/services/socket_service.dart';
import 'package:chatproject/widgets/button_elevate.dart';
import 'package:chatproject/widgets/custom_input.dart';
import 'package:chatproject/widgets/labels.dart';
import 'package:chatproject/widgets/logo.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: SafeArea(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: SizedBox(
            height: MediaQuery.of(context).size.height * 0.9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Logo(
                  title: 'Registro',
                ),
                _Form(),
                const Labels(
                  route: 'login',
                  title: 'Iniciar sesión',
                  subtitle: 'Ya tienes una cuenta',
                ),
              ],
            ),
          ),
        ),
      ),
    ));
  }
}

class _Form extends StatefulWidget {
  @override
  State<_Form> createState() => __FormState();
}

class __FormState extends State<_Form> with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  final emailController = TextEditingController();
  final passController = TextEditingController();
  final userController = TextEditingController();
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void onPressed(context) async {
    final authServices = Provider.of<AuthService>(context, listen: false);
    final sockerService = Provider.of<SocketServices>(context, listen: false);
    FocusScope.of(context).unfocus();

    final repsonse = await authServices.register(
        userController.text, emailController.text, passController.text);
    if (repsonse == true) {
      sockerService.connect();
      Navigator.pushReplacementNamed(context, 'user');
    } else {
      // Mostrar alerta
      alert(context, 'Registro incorrecto', repsonse);
    }
  }

  @override
  Widget build(BuildContext context) {
    final authServices = Provider.of<AuthService>(context);
    
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      margin: const EdgeInsets.only(bottom: 50),
      child: Column(children: [
        CustomInput(
          key: const Key('name'),
          icon: Icons.person_2,
          placeholder: 'name',
          keyboardType: TextInputType.text,
          textController: userController,
        ),
        const SizedBox(height: 10),
        CustomInput(
          key: const Key('email'),
          icon: Icons.email,
          placeholder: 'Email',
          keyboardType: TextInputType.emailAddress,
          textController: emailController,
        ),
        const SizedBox(height: 10),
        CustomInput(
          key: const Key('password'),
          icon: Icons.lock,
          placeholder: 'Password',
          keyboardType: TextInputType.text,
          textController: passController,
          isPassword: true,
        ),
        const SizedBox(
          height: 15,
        ),
        ButtonElevate(
          title: 'Crear cuenta',
          onPress:
              authServices.authenticating ? () => {} : () => onPressed(context),
        )
      ]),
    );
  }
}
