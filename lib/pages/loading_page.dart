import 'package:chatproject/pages/login_page.dart';
import 'package:chatproject/pages/users_pages.dart';
import 'package:chatproject/services/socket_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:chatproject/services/auth_services.dart';

class LoadingPage extends StatelessWidget {
  const LoadingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: checkLoginState(context),
        builder: (context, snapshot) {
          return const Center(
            child: Text('Esperando'),
          );
        },
      ),
    );
  }

  Future<void> checkLoginState(BuildContext context) async {
    final authService = Provider.of<AuthService>(context, listen: false);
    final sockerService = Provider.of<SocketServices>(context, listen: false);
    final autenticado = await authService.isLoggedIn();
    if (autenticado) {
      sockerService.connect();
      Navigator.pushReplacement(
          context,
          PageRouteBuilder(
              transitionDuration: const Duration(milliseconds: 200),
              pageBuilder: (
                _,
                __,
                ___,
              ) =>
                  const UserPage()));
    } else {
      sockerService.disconnect();
      Navigator.pushReplacement(
          context,
          PageRouteBuilder(
              transitionDuration: const Duration(milliseconds: 200),
              pageBuilder: (
                _,
                __,
                ___,
              ) =>
                  const LoginPage()));
    }
  }
}
