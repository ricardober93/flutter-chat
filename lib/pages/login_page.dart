import 'package:chatproject/helpers/alert_message.dart';
import 'package:chatproject/models/login_response.dart';
import 'package:chatproject/services/auth_services.dart';
import 'package:chatproject/services/socket_service.dart';
import 'package:chatproject/widgets/button_elevate.dart';
import 'package:chatproject/widgets/custom_input.dart';
import 'package:chatproject/widgets/labels.dart';
import 'package:chatproject/widgets/logo.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: SafeArea(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: SizedBox(
            height: MediaQuery.of(context).size.height * 0.9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Logo(
                  title: 'Messenger chat',
                ),
                _Form(),
                const Labels(
                  route: 'register',
                  title: 'Crear cuenta nueva',
                  subtitle: 'No tienes una cuenta',
                ),
              ],
            ),
          ),
        ),
      ),
    ));
  }
}

class _Form extends StatefulWidget {
  @override
  State<_Form> createState() => __FormState();
}

class __FormState extends State<_Form> {

  final emailController = TextEditingController();
  final passController = TextEditingController();


onPressed(BuildContext context) async {
    final authServices = Provider.of<AuthService>(context, listen: false);
    final sockerService = Provider.of<SocketServices>(context, listen: false);

    FocusScope.of(context).unfocus();

    final isOk =
        await authServices.login(emailController.text, passController.text);
    if (isOk) {
      sockerService.connect();
      Navigator.pushReplacementNamed(context, 'user');
    } else {
      // Mostrar alerta
      alert(context, 'Login incorrecto', 'Revise sus credenciales');
    }
  }

  @override
  Widget build(BuildContext context) {
    
    final authService = Provider.of<AuthService>(context);
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      margin: const EdgeInsets.only(bottom: 50),
      child: Column(children: [
        CustomInput(
          key: const Key('email'),
          icon: Icons.email,
          placeholder: 'Email',
          keyboardType: TextInputType.emailAddress,
          textController: emailController,
        ),
        const SizedBox(height: 10),
        CustomInput(
          key: const Key('password'),
          icon: Icons.lock,
          placeholder: 'Password',
          keyboardType: TextInputType.text,
          textController: passController,
          isPassword: true,
        ),
        const SizedBox(
          height: 15,
        ),
        ButtonElevate(
          title: 'Ingresar',
          onPress:
              authService.authenticating ? () => {} : () => onPressed(context),
        )
      ]),
    );
  }
}
