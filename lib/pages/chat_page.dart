import 'dart:io';

import 'package:chatproject/services/auth_services.dart';
import 'package:chatproject/services/chat_service.dart';
import 'package:chatproject/services/socket_service.dart';
import 'package:chatproject/widgets/chat_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChatPage extends StatefulWidget {
  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> with TickerProviderStateMixin {
  final textCtr = TextEditingController();
  final _focus = FocusNode();
  var canSend = false;

  ChatService chatService = ChatService();
  SocketServices socketService = SocketServices();
  AuthService authService = AuthService();

  List<ChatMessage> listChat = [];

  @override
  void initState() {
    super.initState();
    chatService = Provider.of<ChatService>(context, listen: false);
    socketService = Provider.of<SocketServices>(context, listen: false);
    authService = Provider.of<AuthService>(context, listen: false);

    socketService.socket.on('private-message', _listenMessage);

    _loadHistory(chatService.userTo!.uid);
  }

  _loadHistory(String uid) async {
    final messages = await chatService.getChat(uid);

    final history = messages.map((m) => ChatMessage(
          text: m.message,
          uid: m.from,
          animationController: AnimationController(
              vsync: this, duration: const Duration(milliseconds: 0))
            ..forward(),
        ));

    setState(() {
      listChat.insertAll(0, history);
    });
  }

  @override
  void dispose() {
    for (ChatMessage message in listChat) {
      message.animationController.dispose();
    }

    socketService.socket.off('private-message');
    super.dispose();
  }

  void _listenMessage(dynamic payload) {
    ChatMessage message = ChatMessage(
      text: payload['message'],
      uid: payload['from'],
      animationController: AnimationController(
          vsync: this, duration: const Duration(milliseconds: 400)),
    );

    setState(() {
      listChat.insert(0, message);
    });

    message.animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pushReplacementNamed(context, 'user'),
        ),
        title: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  backgroundColor: Colors.blue[100],
                  maxRadius: 10,
                  child: Text(chatService.userTo!.name.substring(0, 2),
                      style: const TextStyle(
                          fontSize: 12, fontWeight: FontWeight.w800)),
                ),
                const SizedBox(width: 5),
                Text(chatService.userTo!.name,
                    style: const TextStyle(fontSize: 16)),
              ],
            ),
          ],
        ),
        backgroundColor: Colors.blue[900],
      ),
      body: Column(
        children: [
          Flexible(
            child: ListView.builder(
              physics: const BouncingScrollPhysics(),
              itemCount: listChat.length,
              itemBuilder: (_, i) => listChat[i],
              reverse: true,
            ),
          ),
          const Divider(height: 1, color: Colors.black),
          _inputChat(),
        ],
      ),
    );
  }

  Container _inputChat() {
    return Container(
      margin: const EdgeInsets.only(left: 10, right: 10),
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: textCtr,
              onSubmitted: (value) {
                print(value);
                textCtr.clear();
              },
              onChanged: (value) {
                setState(() {
                  if (value.trim().length > 0) {
                    canSend = true;
                  } else {
                    canSend = false;
                  }
                });
              },
              focusNode: _focus,
              decoration: const InputDecoration(
                hintText: 'Enviar mensaje',
                border: InputBorder.none,
              ),
            ),
          ),
          const SizedBox(width: 10),
          Container(
              margin: EdgeInsets.only(right: Platform.isIOS ? 0 : 4),
              child: Platform.isIOS
                  ? CupertinoButton(
                      onPressed: textCtr.text.isEmpty
                          ? null
                          : () => _hanldeSubmit(textCtr.text),
                      child: Text('Enviar',
                          style: TextStyle(color: Colors.blue[600])),
                    )
                  : IconTheme(
                      data: IconThemeData(color: Colors.blue[600]),
                      child: IconButton(
                        highlightColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        onPressed: textCtr.text.isEmpty
                            ? null
                            : () => _hanldeSubmit(textCtr.text),
                        icon: const Icon(Icons.send),
                      ),
                    ))
        ],
      ),
    );
  }

  _hanldeSubmit(String value) {
    textCtr.clear();
    _focus.requestFocus();

    final newMessage = ChatMessage(
      uid: authService.user!.user!.uid,
      text: value,
      animationController: AnimationController(
          vsync: this, duration: const Duration(milliseconds: 400)),
    );
    listChat.insert(0, newMessage);
    newMessage.animationController.forward();
    setState(() {
      canSend = false;
    });

    socketService.socket.emit('private-message', {
      'from': authService.user?.user?.uid,
      'to': chatService.userTo?.uid,
      'message': value
    });
  }
}
