import 'package:chatproject/routes/routes.dart';
import 'package:chatproject/services/auth_services.dart';
import 'package:chatproject/services/chat_service.dart';
import 'package:chatproject/services/socket_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthService()),
        ChangeNotifierProvider(create: (_) => SocketServices()),
        ChangeNotifierProvider(create: (_) => ChatService())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'chat Demo',
        theme: ThemeData.dark(),
        initialRoute: 'loading',
        routes: appRoutes,
      ),
    );
  }
}
