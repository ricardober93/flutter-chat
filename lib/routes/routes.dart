import 'package:chatproject/pages/chat_page.dart';
import 'package:chatproject/pages/loading_page.dart';
import 'package:chatproject/pages/login_page.dart';
import 'package:chatproject/pages/register_page.dart';
import 'package:chatproject/pages/users_pages.dart';
import 'package:flutter/material.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'user': (_) => UserPage(),
  'login': (_) => LoginPage(),
  'chat': (_) => ChatPage(),
  'register': (_) => RegisterPage(),
  'loading': (_) => LoadingPage(),
};
