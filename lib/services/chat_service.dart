import 'package:chatproject/global/enviroment.dart';
import 'package:chatproject/services/auth_services.dart';
import 'package:http/http.dart' as http;

import 'package:chatproject/models/chat_response.dart';
import 'package:chatproject/models/user.dart';
import 'package:flutter/material.dart';

class ChatService with ChangeNotifier {
  User? userTo;

  Future<List<Message>> getChat(String userId) async {
    final token = await AuthService.getToken();

    final resp = await http.get(
        Uri.parse('${EnvironmentConfig.apiUrl}/messages/$userId'),
        headers: {
          'Content-Type': 'application/json',
          'x-token': token,
        });

    final chatResponse = chatResponseFromJson(resp.body);

    return chatResponse.messages;
  }
}
