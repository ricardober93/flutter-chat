import 'dart:convert';

import 'package:chatproject/global/enviroment.dart';
import 'package:chatproject/models/user.dart';
import 'package:chatproject/models/user_response.dart';
import 'package:chatproject/services/auth_services.dart';
import 'package:http/http.dart' as http;

class UserService {
  Future<List<User>> getUser() async {
    final token = await AuthService.getToken();

    try {
      final resp = await http.get(
          Uri.parse('${EnvironmentConfig.apiUrl}/users'),
          headers: {'Content-Type': 'application/json', 'x-token': token});

      if (resp.statusCode == 200) {
        final userResponse = userResponseFromJson(resp.body);
        return userResponse.users;
      } else {
        throw Exception('Failed to load user');
      }
    } catch (e) {
      return [];
    }
  }
}
