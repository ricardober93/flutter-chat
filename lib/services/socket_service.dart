import 'package:chatproject/global/enviroment.dart';
import 'package:chatproject/services/auth_services.dart';
import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as io;

enum ServerStatus { online, offline, connecting }

class SocketServices with ChangeNotifier {
  // Socket IO
  ServerStatus _serverStatus = ServerStatus.connecting;

  ServerStatus get serverStatus => _serverStatus;

  io.Socket? _socket;
  io.Socket get socket => _socket!;

  void connect() async {
    final token = await AuthService.getToken();

    _socket = io.io(EnvironmentConfig.socketUrl, {
      'transports': ['websocket'],
      'autoConnect': true,
      'forceNew': true,
      'extraHeaders': {'x-token': token},
    });

    _socket?.onConnect((_) {
      _serverStatus = ServerStatus.online;
      print('conectado');
      notifyListeners();
    });
  }

  void disconnect() {
    _socket?.onDisconnect((_) {
      _serverStatus = ServerStatus.offline;
      notifyListeners();
    });
  }
}
