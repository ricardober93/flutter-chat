import 'dart:convert' as convert;

import 'package:chatproject/models/login_response.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import 'package:chatproject/global/enviroment.dart';

import 'package:flutter/material.dart';

class AuthService with ChangeNotifier {
  Login? user;
  bool _authenticating = false;

  final _storage = const FlutterSecureStorage();

  bool get authenticating => _authenticating;
  set authenticating(bool valor) {
    _authenticating = valor;
    notifyListeners();
  }

  static Future<String> getToken() async {
    const storage = FlutterSecureStorage();
    final token = await storage.read(key: 'token');
    return token ?? '';
  }

  static Future deleteToken() async {
    const storage = FlutterSecureStorage();
    await storage.delete(key: 'token');
  }

  Future<bool> login(String email, String password) async {
    authenticating = true;
    final data = {
      'email': email,
      'password': password,
    };

    final resp = await http.post(Uri.parse('${EnvironmentConfig.apiUrl}/login'),
        body: convert.jsonEncode(data),
        headers: {'Content-Type': 'application/json'});

    authenticating = false;
    if (resp.statusCode == 200) {
      final res = loginFromJson(resp.body);
      user = res;
      await _saveToken(res.token);

      //save token in storage
      return true;
    } else {
      return false;
    }
  }

  Future<bool> isLoggedIn() async {
    final token = await _storage.read(key: 'token');
    final resp = await http.get(
        Uri.parse('${EnvironmentConfig.apiUrl}/login/renew'),
        headers: {'Content-Type': 'application/json', 'x-token': token ?? ''});

    if (resp.statusCode == 200) {
      final res = loginFromJson(resp.body);
      user = res;
      await _saveToken(res.token);

      //save token in storage
      return true;
    } else {
      logout();
      return false;
    }
  }

  Future register(String name, String email, String password) async {
    authenticating = true;
    final data = {
      'name': name,
      'email': email,
      'password': password,
    };

    final resp = await http.post(
        Uri.parse('${EnvironmentConfig.apiUrl}/login/new'),
        body: convert.jsonEncode(data),
        headers: {'Content-Type': 'application/json'});

    authenticating = false;
    if (resp.statusCode == 200) {
      final res = loginFromJson(resp.body);
      user = res;
      //save token in storage
      await _saveToken(res.token);

      return true;
    } else {
      final res = convert.jsonDecode(resp.body);
      return res['msg'];
    }
  }

  Future _saveToken(String token) async {
    await _storage.write(key: 'token', value: user!.token);
  }

  logout() async {
    await _storage.delete(key: 'token');
  }
}
